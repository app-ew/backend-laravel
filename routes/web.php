<?php

use App\Manufacture;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('//', function () {
    return view('welcome');
});

Route::get('/manufacture', function () {
    return view('manufacture', ['items' => Manufacture::with('User')->all()]);
});

Route::get('/manufacture/{id}', function ($id) {
    return view('manufacture', ['items' => Manufacture::with('User')->find($id)]);
});
