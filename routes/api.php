<?php

use App\Machinery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'amend'
], function ($router) {
    Route::post('auth', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group([
    //'middleware' => 'auth',
] , function ($router) {


    Route::get('/manufacture', 'ManufactoryController@getManufactureList');
    Route::get('/hierarchy', 'HierarchyController@getHierarchy');

    Route::get('/manufacture/{id}', 'ManufactoryController@getManufacture');
    Route::post('/manufacture', 'ManufactoryController@postManufacture');
    Route::put('/manufacture/{id}', 'ManufactoryController@updateManufacture');
    Route::delete('/manufacture/{id}', 'ManufactoryController@deleteManufacture');
    Route::get('/manufacture/{id}/machinery', function ($id) {
        return Machinery::where('idManufacture', $id)->get();
    });

//Route::get('/manufacture/{id}/machinery-group', function ($id) {
//    return Machinerygroup::where('idManufacture', $id)->get();
//});
    Route::get('/manufacture/{id}/machinery-group', 'MachineryGroupController@getMachineryGroupList');
    Route::post('/manufacture/{id}/machinery-group', 'MachineryGroupController@postMachineryGroup');
    Route::get('/manufacture/{id}/machinery-group/{idMachineryGroup}', 'MachineryGroupController@getMachineryGroup');
    Route::put('/manufacture/{id}/machinery-group/{idMachineryGroup}', 'MachineryGroupController@updateMachineryGroup');
    Route::delete('/manufacture/{id}/machinery-group/{idMachineryGroup}', 'MachineryGroupController@deleteMachineryGroup');

//Route::get('/machinery-group/{idMachineryGroup}/machinery', function ($idMachineryGroup) {
    //   return Machinery::where('idMachinerygroup', $idMachineryGroup)->get();
//});
    Route::get('/machinery/query', 'MachineryController@getMachineryByIdMachinerygroupIds');
    Route::get('/machinery-group', 'MachineryGroupController@getMachineryGroupListByIds');
    Route::get('/machinery-group/{idMachineryGroup}/machinery', 'MachineryController@getMachineryList');
    Route::get('/machinery/{idMachinery}', 'MachineryController@getMachineryById');
    Route::get('/machinery', 'MachineryController@getMachineryAll');
    Route::post('/machinery', 'MachineryController@postMachinery');
    Route::put('/machinery/{idMachinery}', 'MachineryController@updateMachinery');
    Route::delete('/machinery/{idMachinery}', 'MachineryController@deleteMachinery');

    Route::get('/hoses/{idManufacture}/{idMachinerygroup}/{idMachinery}', 'HoseController@getHoseList');
    Route::get('/machinery/{idMachinery}/hoses', 'HoseController@getHoseListByMachinery');
    Route::get('/machinery/{idMachinery}/hoses/{idHose}', 'HoseController@getHoseById');
    Route::post('/machinery/{idMachinery}/hoses', 'HoseController@postHose');
    Route::put('/machinery/{idMachinery}/hoses/{idHose}', 'HoseController@updateHose');
    Route::delete('/machinery/{idMachinery}/hoses/{idHose}', 'HoseController@deleteHose');

    Route::get('/manufacture/{id}/contacts', 'ContactsController@getContactList');
    Route::post('/manufacture/{id}/contacts', 'ContactsController@postContact');
    Route::put('/manufacture/{id}/contacts/{idContac}', 'ContactsController@updateContact');
    Route::delete('/manufacture/{id}/contacts/{idContac}', 'ContactsController@deleteContact');

    Route::get('/hose-data/type', 'DataHoseController@getHoseType');
    Route::get('/hose-data/dn', 'DataHoseController@getHosesDn');
    Route::get('/hose-data/level-danger', 'DataHoseController@getHosesLevelDanger');
    Route::get('/hose-data/mounting', 'DataHoseController@getHosesMounting');
    Route::get('/hose-data/pipe-connection', 'DataHoseController@getHosesPipeConnection');
    Route::get('/hose-data/vd', 'DataHoseController@getHoseVd');

    Route::get('/messages/hoses/{idHose}', 'HoseTestController@getHoseTests');
    Route::get('/messages/hoses', 'HoseTestController@getHoseTestList');
    Route::post('/messages/hoses', 'HoseTestController@postHoseTest');

    Route::post('/files/hoses/upload', 'HoseController@addHoseCertificate');
    Route::post('/files/elements/upload', 'ElementController@addElementCertificate');
    Route::post('/files/elements/delete', 'ElementController@removeElementCertificate');

    Route::get('/users', 'UserController@getUserList');

    Route::get('/hose-data/{table}', 'DataHoseController@getData');
    Route::get('/hose-data/{table}/{id}', 'DataHoseController@getDataById');
    Route::post('/hose-data', 'DataHoseController@createData');
    Route::put('/hose-data', 'DataHoseController@updateData');
    Route::post('/hose-data/delete', 'DataHoseController@deleteData');

    Route::get('/element', 'ElementController@getAll');
    Route::get('/elements/dashboard', 'ElementController@getAllDashboard');
    Route::get('/machinery/{id}/element', 'ElementController@getByIdMachine');
    Route::post('/element', 'ElementController@create');
    Route::get('/element/{id}', 'ElementController@getById');
    Route::put('/element/{id}', 'ElementController@update');
    Route::delete('/element/{id}', 'ElementController@delete');

    Route::get('/element/{id}/replace', 'ElementReplaceController@index');
    Route::post('/element/{id}/replace', 'ElementReplaceController@store');
    Route::put('/element/{id}/replace', 'ElementReplaceController@update');
    Route::delete('/element/{id}/replace', 'ElementReplaceController@destroy');

    Route::get('/element/{id}/test', 'ElementTestController@index');
    Route::post('/element/{id}/test', 'ElementTestController@store');
    Route::put('/element/{id}/test', 'ElementTestController@update');
    Route::delete('/element/{id}/test', 'ElementTestController@destroy');

});

/*\DB::listen(function($query) {
    var_dump($query->sql);
});*/
