<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, int $idMachinery)
 */
class Hose extends Model
{
    use Notifiable;

    protected $primaryKey = "id";

    protected $fillable = [
        "idMachinery",
        "idMachinerygroup",
        "Article",
        "idType",
        "idDn",
        "idLArm",
        "idLRohrAnschlub",
        "idRArm",
        "idRRohrAnschlub",
        "idVd",
        "Notice",
        "idLevelDanger",
        "Status",
        "DateManufacturing",
        "DateTest",
        "DateNextTest",
        "DateReplacement",
        "Certificatepath",
        "IntervalView",
        "IntervalReplace",
        "Position",
        "InstallationLocation",
        "Pressure",
        "Length",
        "IdNumber",
        "IdPress",
        "InstrNo",
        "DegreeL",
        "DegreeR",
    ];

    protected $with = ['Machinery', 'Machinerygroup', 'Type', 'LArm', 'LRohrAnschlub', 'RArm', 'RRohrAnschlub', 'Vd', 'LevelDanger'];

    public function Machinery()
    {
        return $this->belongsTo('App\Machinery', 'idMachinery');
    }

    public function Machinerygroup()
    {
        return $this->belongsTo('App\Machinerygroup', 'idMachinerygroup');
    }

    public function Type()
    {
        return $this->belongsTo('App\DataHosesType', 'idType');
    }

    public function LArm()
    {
        return $this->belongsTo('App\DataHosesMounting', 'idLArm');
    }

    public function LRohrAnschlub()
    {
        return $this->belongsTo('App\DataHosesPipeConnection', 'idLRohrAnschlub');
    }

    public function Concatenation()
    {
        return $this->belongsTo('App\DataHosesMounting', 'idConcatenation');
    }

    public function RArm()
    {
        return $this->belongsTo('App\DataHosesMounting', 'idRArm');
    }

    public function RRohrAnschlub()
    {
        return $this->belongsTo('App\DataHosesPipeConnection', 'idRRohrAnschlub');
    }

    public function Vd()
    {
        return $this->belongsTo('App\DataVD', 'idVd');
    }

    public function LevelDanger()
    {
        return $this->belongsTo('App\DataHosesLevelDanger', 'idLevelDanger');
    }

    public function HosesTestList()
    {
        return $this->hasMany('App\HoseTest', 'idHose');
    }
}
