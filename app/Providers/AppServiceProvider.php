<?php

namespace App\Providers;

use App\Http\Resources\HoseResources;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Model::$snakeAttributes = false;
        HoseResources::withoutWrapping();
        //Model::resolveRelationUsing();
    }
}
