<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementReplace extends Model
{
    protected $fillable = [
        'idElement',
        'Notice',
        'DateReplace',
        'DateNextReplace',
    ];
}
