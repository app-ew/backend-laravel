<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementType extends Model
{
    protected $fillable = ['Title'];
}
