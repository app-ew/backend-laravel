<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class HoseTest extends Model
{
    use Notifiable;

    protected $primaryKey = "id";

    protected $fillable = [
        "idHose",
        "Status",
        "Notice",
        "DateNextTest",
    ];

    public function Hose()
    {
        return $this->belongsTo('App\Hose', 'idHose');
    }
}
