<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use Notifiable;

    protected $primaryKey ="id";

    protected $fillable = [
        "Login",
        "Pass",
        "FIO",
        "Phone",
        "Email",
        "Title",
    ];

    public function Machinerygroups()
    {
        //return $this->hasMany('App\Machinerygroup');
        return $this->belongsToMany('App\Machinerygroup', 'App\ContactMachinerygroup', 'idMachinerygroup', 'idContact');
    }
}
