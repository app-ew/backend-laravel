<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $idManufacture)
 */
class Machinerygroup extends  Model
{
    use Notifiable;

    protected $primaryKey ="id";

    protected $fillable = [
        "idUser",
        "idManufacture",
        "Title",
        "Address"
    ];

    protected $with = [];

    public function User()
    {
        return $this->belongsTo('App\User', 'idUser');
    }

    public function Manufacture()
    {
        return $this->belongsTo('App\Manufacture', 'idManufacture');
    }

    public function MachineryList()
    {
        return $this->hasMany('App\Machinery', 'idMachinerygroup');
    }

    public function ContactList()
    {
        return $this->belongsToMany('App\Contact', 'contact_machinerygroups', 'idContact', 'idMachinerygroup');
    }
}
