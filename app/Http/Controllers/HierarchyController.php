<?php

namespace App\Http\Controllers;

use App\Machinery;
use App\Machinerygroup;
use App\Manufacture;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Array_;

class HierarchyController extends Controller
{
    function getHierarchy()
    {
        $manufacture = Manufacture::all();
        $machinerygroup = Machinerygroup::all();
        $machinery = Machinery::all();

        $res = [];

        for ($i = 0; $i < count($manufacture); $i++) {

            $manufactureItem = [
                'id' => $manufacture[$i]->id,
                'Type' => 'Manufacture',
                'Title' => $manufacture[$i]->Title,
                'items' => [],
            ];

            for ($j = 0; $j < count($machinerygroup); $j++) {
                if ($manufactureItem['id'] == $machinerygroup[$j]->idManufacture) {
                    $machinerygroupItem = [
                        'id' => $machinerygroup[$j]->id,
                        'Type' => 'Machinerygroup',
                        'Title' => $machinerygroup[$j]->Title,
                        'items' => [],
                    ];

                    for ($k = 0; $k < count($machinery); $k++) {
                        if ($machinerygroupItem['id'] == $machinery[$k]->idMachinerygroup) {
                            $machineryItem = [
                                'id' => $machinery[$k]->id,
                                'Type' => 'Machinery',
                                'Title' => $machinery[$k]->Title,
                                'items' => [],
                            ];
                            array_push($machinerygroupItem['items'], $machineryItem);
                        }
                    }


                    array_push($manufactureItem['items'], $machinerygroupItem);
                }
            }

            array_push($res, $manufactureItem);
        }

        return $res;
    }
}
