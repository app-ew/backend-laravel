<?php

namespace App\Http\Controllers;

use App\ElementTest;
use Illuminate\Support\Facades\Request;

class ElementTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idElement)
    {
        return ElementTest::where('idElement', $idElement)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    public function store(Request $request)
    {
        $input = Request::all();
        $item = new ElementTest($input);
        $item->save();

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ElementReplace $elementTest
     * @return ElementTest
     */
    public function show(ElementTest $elementTest)
    {
        return $elementTest;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ElementReplace $elementReplce
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::all();
        $item = ElementTest::find($id);

        return $item->update($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ElementReplace $elementReplce
     * @return bool
     */
    public function destroy($id)
    {
        return ElementTest::find($id)->delete();
    }
}
