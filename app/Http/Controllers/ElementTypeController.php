<?php

namespace App\Http\Controllers;

use App\ElementType;
use Illuminate\Http\Request;

class ElementTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ElementType  $elementType
     * @return \Illuminate\Http\Response
     */
    public function show(ElementType $elementType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ElementType  $elementType
     * @return \Illuminate\Http\Response
     */
    public function edit(ElementType $elementType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ElementType  $elementType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ElementType $elementType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ElementType  $elementType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ElementType $elementType)
    {
        //
    }
}
