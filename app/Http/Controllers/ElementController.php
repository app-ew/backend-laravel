<?php

namespace App\Http\Controllers;

use App\Element;
use App\ElementReplace;
use App\Http\Resources\ElementResource;
use App\Http\Resources\ElementResourceDashboard;
use App\Http\Resources\ElementResourceFull;
use App\Machinerygroup;
use Illuminate\Support\Facades\Request;

class ElementController extends Controller
{
    function getAll()
    {
        $input = Request::all();

        $idManufacture = $input['idManufacture'] ?? null;
        $idMachinerygroup = $input['idMachinerygroup'] ?? null;
        $idMachinery = $input['idMachinery'] ?? null;

        $res1 = [];
        $res2 = [];
        $res3 = [];

        if ($idManufacture) {
            $machinerygroups = Machinerygroup::whereIn('idManufacture', $idManufacture)->pluck('id');
            $res1 = ElementResource::collection(Element::whereIn('idMachinerygroup', $machinerygroups)->get())->all();
        }

        if ($idMachinerygroup) {
            $res2 = ElementResource::collection(Element::whereIn('idMachinerygroup', $idMachinerygroup)->get())->all();
        }

        if ($idMachinery) {
            $res3 = ElementResource::collection(Element::whereIn('idMachinery', $idMachinery)->get())->all();
        }

        return array_merge($res1, $res2, $res3);
    }

    function getAllDashboard()
    {
        $input = Request::all();

        $idManufacture = $input['idManufacture'] ?? null;
        $idMachinerygroup = $input['idMachinerygroup'] ?? null;
        $idMachinery = $input['idMachinery'] ?? null;

        $res1 = [];
        $res2 = [];
        $res3 = [];

        if ($idManufacture) {
            $machinerygroups = Machinerygroup::whereIn('idManufacture', $idManufacture)->pluck('id');
            $res1 = ElementResourceDashboard::collection(Element::whereIn('idMachinerygroup', $machinerygroups)->get())->all();
        }

        if ($idMachinerygroup) {
            $res2 = ElementResourceDashboard::collection(Element::whereIn('idMachinerygroup', $idMachinerygroup)->get())->all();
        }

        if ($idMachinery) {
            $res3 = ElementResourceDashboard::collection(Element::whereIn('idMachinery', $idMachinery)->get())->all();
        }

        return array_merge($res1, $res2, $res3);
    }

    function getById($id)
    {
        $element = new ElementResourceFull(Element::find($id));

        return $element;
    }

    function getByIdMachine($id)
    {
        $elements = ElementResourceFull::collection(Element::where('idMachinery', $id)->get());

        return $elements;
    }

    function create()
    {
        $input = Request::all();
        $item = new Element($input);
        $item->save();
        return $item;
    }

    function update($id)
    {
        $input = Request::all();
        return Element::find($id)->update($input);
    }

    function delete($id)
    {
        return Element::find($id)->delete();
    }


    public function addElementCertificate()
    {
        $input = Request::all();

        $idElement = $input['idElement'];
        $file = $input['Certificatepath'];

        $element = Element::find($idElement);

        if ($file !== null) {
            $original_name = $idElement . '.' . $file->getClientOriginalExtension();
            $file->move(public_path() . '/certificates', $original_name);
            $element->Certificatepath = 'certificates/' . $original_name;
        }
        $element->save();
        return new ElementResourceFull($element);
    }

    public function removeElementCertificate()
    {
        $input = Request::all();

        $idElement = $input['idElement'];

        $element = Element::find($idElement);

        $element->Certificatepath = '';
        $element->save();
        return new ElementResourceFull($element);
    }
}
