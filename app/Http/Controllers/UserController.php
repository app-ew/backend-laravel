<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    public function getUserList()
    {
        return User::all();
    }

}
