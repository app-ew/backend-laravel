<?php

namespace App\Http\Controllers;

use App\Element;
use App\ElementReplace;
use Illuminate\Support\Facades\Request;

class ElementReplaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idElement)
    {
        return ElementReplace::where('idElement', $idElement)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    public function store(Request $request)
    {
        $input = Request::all();
        $item = new ElementReplace($input);
        $item->save();

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ElementReplace $elementReplace
     * @return ElementReplace
     */
    public function show(ElementReplace $elementReplace)
    {
        return $elementReplace;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ElementReplace $elementReplce
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::all();
        $item = ElementReplace::find($id);

        return $item->update($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ElementReplace $elementReplce
     * @return bool
     */
    public function destroy($id)
    {
        return ElementReplace::find($id)->delete();
    }
}
