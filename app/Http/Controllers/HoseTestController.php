<?php

namespace App\Http\Controllers;

use App\HoseTest;
use App\Http\Resources\HoseTestResource;
use App\Hose;
use App\Manufacture;
use App\Machinery;
use app\Models\HoseEdit;
use Illuminate\Support\Facades\Request;

class HoseTestController extends Controller
{
    public function getHoseTests($idHose) // все проверки по шлангу
    {
        $input = Request::all();

        return HoseTestResource::collection(\App\HoseTest::where('idHose', $idHose)->get());
    }

    public function getHoseTestList() // все проверки
    {
        $input = Request::all();

        return HoseTestResource::collection(\App\HoseTest::all()->take(100000));
    }

    public function postHoseTest()
    {
        $input = Request::all();

        $idHose = $input['idHose'];

        $hosetest = HoseTest::create($input);
        $hose = Hose::find($idHose)->update($input);

        return response()->json($hosetest, 201);
    }
}
