<?php

namespace App\Http\Resources;

use App\ElementReplace;
use App\ElementTest;
use Illuminate\Http\Resources\Json\JsonResource;

class ElementResourceFull extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $article = $this->Article;
        $intervalTest = 0;
        $intervalReplace = 0;

        try {
            $json = json_decode($this->JSON);

            if ($this->idType == 1) {
                $article = '';

                $article .= ' ' . $json->idLevelDanger;
                if ($this->Type) $article .= $json->Type;
                $article .= ' ' . $json->Width;
                $article .= ' ' . $json->Length;
                $article .= ' ' . $json->Pressure;
                $article .= ' ' . $json->LType;
                $article .= ' ' . $json->LSize;
                $article .= ' ' . $json->LDegree;
                $article .= ' ' . $json->RType;
                $article .= ' ' . $json->RSize;
                $article .= ' ' . $json->RDegree;
                $article .= ' ' . $json->VD;

                $intervalTest = $json->IntervalView;
                $intervalReplace = $json->IntervalReplace;
            }

            if ($this->idType == 2) {
                $article = '';

                $article .= ' ' . $json->Type;
                $article .= ' ' . $json->VolumeL;
                $article .= ' ' . $json->Pressure;
                $article .= ' ' . $json->InstallationLocation;
            }
        } finally {

            $elementReplaces = ElementReplace::where('idElement', $this->id)->get();
            $elementTests = ElementTest::where('idElement', $this->id)->get();

            $lastTest = collect($elementTests)->sortBy('DateTest')->first();
            $lastReplace = collect($elementReplaces)->sortBy('DateReplace')->first();

            $DateLastTest = '';
            $DateNextTest = '';

            $DateLastReplace = '';
            $DateNextReplace = '';

            if ($lastTest) {
                $DateLastTest = $lastTest->DateTest;

                if ($lastTest->DateNextTest) {
                    $DateNextTest = $lastTest->DateNextTest;
                } else {
                    $date = strtotime($lastTest->DateTest);
                    $DateNextTest = date("Y-m-d", strtotime("+$intervalTest months", $date));
                }
            }

            if ($lastReplace) {
                $DateLastReplace = $lastReplace->DateReplace;

                if ($lastReplace->DateNextReplace) {
                    $DateNextReplace = $lastReplace->DateNextReplace;
                } else {
                    $date = strtotime($lastReplace->DateReplace);
                    $DateNextReplace = date("Y-m-d", strtotime("+$intervalReplace months", $date));
                }
            }

            return [
                "id" => $this->id,
                "idMachinery" => $this->idMachinery,
                "idMachinerygroup" => $this->idMachinerygroup,
                "Article" => $article,
                "idType" => $this->idType,
                "Type" => $this->Type,
                "JSON" => $this->JSON,
                "Replaces" => $elementReplaces,
                "Tests" => $elementTests,
                "Certificatepath" => $this->Certificatepath,
                "DateLastTest" => $DateLastTest,
                "DateNextTest" => $DateNextTest,
                "DateLastReplace" => $DateLastReplace,
                "DateNextReplace" => $DateNextReplace,
            ];
        }
    }
}
