<?php

namespace App\Http\Resources;

use App\ElementReplace;
use App\ElementTest;
use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class ElementResourceDashboard extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $articleNumber = '';
        $article = $this->Article;
        $intervalTest = 0;
        $intervalReplace = 0;
        $DateInstall = null;

        try {
            $json = json_decode($this->JSON);

            if ($this->idType == 1) {
                $article = '';

                $article .= ' ' . $json->idLevelDanger;
                if ($this->Type) $article .= $json->Type;
                $article .= ' ' . $json->Width;
                $article .= ' ' . $json->Length;
                $article .= ' ' . $json->Pressure;
                $article .= ' ' . $json->LType;
                $article .= ' ' . $json->LSize;
                $article .= ' ' . $json->LDegree;
                $article .= ' ' . $json->RType;
                $article .= ' ' . $json->RSize;
                $article .= ' ' . $json->RDegree;
                $article .= ' ' . $json->VD;

                $intervalTest = $json->IntervalView;
                $intervalReplace = $json->IntervalReplace;
            }
            if ($this->idType == 2) {
                $article = '';

                $article .= ' ' . $json->Type;
                $article .= ' ' . $json->VolumeL;
                $article .= ' ' . $json->Pressure;
                $article .= ' ' . $json->InstallationLocation;
            }

            $articleNumber = $this->Machinery->Title . '  - ' . $json->InstallationLocation;
            $DateInstall = $json->DateInstall;

        } finally {

            $elementReplaces = ElementReplace::where('idElement', $this->id)->get();
            $elementTests = ElementTest::where('idElement', $this->id)->get();

            $lastTest = collect($elementTests)->sortBy('DateTest')->last();
            $lastReplace = collect($elementReplaces)->sortBy('DateReplace')->last();

            $DateLastTest = '';
            $DateNextTest = '';

            $DateLastReplace = '';
            $DateNextReplace = '';

            if ($lastTest) {
                $DateLastTest = $lastTest->DateTest;

                if ($lastTest->DateNextTest) {
                    $DateNextTest = $lastTest->DateNextTest;
                } else {
                    $date = strtotime($lastTest->DateTest);
                    $DateNextTest = date("Y-m-d", strtotime("+$intervalTest months", $date));
                }
            }

            if ($lastReplace) {
                $DateLastReplace = $lastReplace->DateReplace;

                if ($lastReplace->DateNextReplace) {
                    $DateNextReplace = $lastReplace->DateNextReplace;
                } else {
                    $date = strtotime($lastReplace->DateReplace);
                    $DateNextReplace = date("Y-m-d", strtotime("+$intervalReplace months", $date));
                }
            }

            return [
                "id" => $this->id,
                "idManufacture" => $this->Machinerygroup->idManufacture,
                "idMachinery" => $this->idMachinery,
                "idMachinerygroup" => $this->idMachinerygroup,
                "ArticleNumber" => $articleNumber,
                "Article" => $article,
                "idType" => $this->idType,
                "Type" => $this->Type,
                "Сertificatepath" => $this->Сertificatepath,
                "DateLastTest" => $DateLastTest,
                "DateNextTest" => $DateNextTest,
                "DateLastReplace" => $DateLastReplace,
                "DateNextReplace" => $DateNextReplace,
                "Notice" => $this->Notice,
                "DateInstall" => $DateInstall,
            ];

        }
    }
}
