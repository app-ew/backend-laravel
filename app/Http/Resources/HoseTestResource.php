<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HoseTestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idMachinery' => $this->Hose->idMachinery,
            'idManufacture' => $this->Hose->Machinerygroup->idManufacture,
            'idHose' => $this->idHose,
            'ManufactureTitle' => $this->Hose->Machinerygroup->Manufacture->Title,
            'HoseArticle' => $this->Hose->Article,
            'Status' => $this->Status,
            'Notice' => $this->Notice,
            'DateNextTest' => $this->DateNextTest,
        ];
    }
}
