<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HoseResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $article = '';

        if ($this->Type) $article .= $this->Type->Title;
        $article .= ' '.$this->Length;
        if ($this->LRohrAnschlub) $article .= ' '.$this->LRohrAnschlub->Title;
        if ($this->RRohrAnschlub) $article .= ' / '.$this->RRohrAnschlub->Title;

        return [
            'id' => $this->id,
            'idMachinery' => $this->idMachinery,
            'idMachinerygroup' => $this->idMachinerygroup,
            'idManufacture' => $this->MachineryGroup->id,
            'Machinery' => $this->Machinery,
            'MachineryGroup' => $this->MachineryGroup,
            'Article' => $article,
            'Type' => $this->Type ? $this->Type->Title : null,
            'Dn' => $this->Dn,
            'LArm' => $this->LArm ? $this->LArm->Title : null,
            'LRohrAnschlub' => $this->LRohrAnschlub ? $this->LRohrAnschlub->Title : null,
            'RArm' => $this->RArm ? $this->RArm->Title : null,
            'RRohrAnschlub' => $this->RRohrAnschlub ? $this->RRohrAnschlub->Title : null,
            'Vd' => $this->Vd,
            'Notice' => $this->Notice,
            'LevelDanger' => $this->LevelDanger ? $this->LevelDanger->Title : null,
            'Status' => $this->Status,
            'DateManufacturing' => $this->DateManufacturing, //дата установки
            'DateTest' => $this->DateTest, //дата проверки последней
            'DateNextTest' => $this->DateNextTest, //дата проверки следующей
            'DateReplacement' => $this->DateReplacement, //дата плановой замены
            'Certificatepath' => $this->Certificatepath,

            'IntervalView' => $this->IntervalView,
            'IntervalReplace' => $this->IntervalReplace,
            'Position' => $this->Position,
            'InstallationLocation' => $this->InstallationLocation,
            'Pressure' => $this->Pressure,
            'Length' => $this->Length,
            'IdNumber' => $this->IdNumber,
            'IdPress' => $this->IdPress,
            'InstrNo' => $this->InstrNo,
            'Concatenation' => $this->Concatenation,

            'DegreeL' => $this->DegreeL,
            'DegreeR' => $this->DegreeR,

        ];
    }
}
