<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Manufacture extends Model
{
    use Notifiable;

    protected $fillable = [
        "idUser",
        "Phone",
        "Email",
        "Title",
        "Address",
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'idUser');
    }
}
