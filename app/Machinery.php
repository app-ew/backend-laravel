<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Machinery extends Model
{
    use Notifiable;

    protected $primaryKey ="id";

    protected $fillable = [
        "idManufacture",
        "idMachinerygroup",
        "Title"
    ];

    protected $with = [];

    public function Machinerygroup()
    {
        return $this->belongsTo('App\Machinerygroup', "idMachinerygroup");
    }

}
