<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementTest extends Model
{
    protected $fillable = [
        'idElement',
        'Status',
        'Notice',
        'DateTest',
        'DateNextTest',
    ];
}
