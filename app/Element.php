<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $fillable = [
        "idMachinery",
        "idMachinerygroup",
        "Article",
        "idType",
        "JSON",
        "Сertificatepath"
    ];

    protected $with = ['Machinery', 'Machinerygroup', 'Type'];

    protected $appends = ['DateNextTest', 'DateNextReplace'];


    public function Machinery()
    {
        return $this->belongsTo('App\Machinery', 'idMachinery');
    }

    public function Machinerygroup()
    {
        return $this->belongsTo('App\Machinerygroup', 'idMachinerygroup');
    }

    public function Type()
    {
        return $this->belongsTo('App\ElementType', 'idType');
    }

    public function getDateNextTestAttribute()
    {
        $intervalTest  = 0;

        try {
            $json = json_decode($this->JSON);
            $intervalTest = $json->IntervalView;
        } finally {
            $elementTests = ElementTest::where('idElement', $this->id)->get();
            $lastTest = collect($elementTests)->sortBy('DateTest')->first();
            $DateNextTest = '';

            if ($lastTest) {
                if ($lastTest->DateNextTest) {
                    $DateNextTest = $lastTest->DateNextTest;
                } else {
                    $date = strtotime($lastTest->DateTest);
                    $DateNextTest = date("Y-m-d", strtotime("+$intervalTest months", $date));
                }
            }

            return $DateNextTest;
        }
    }

    public function getDateNextReplaceAttribute()
    {
        $interval  = 0;

        try {
            $json = json_decode($this->JSON);
            $interval = $json->IntervalReplace;
        } finally {
            $element = ElementReplace::where('idElement', $this->id)->get();
            $lastTest = collect($element)->sortBy('DateReplace')->first();
            $DateNextReplace = '';

            if ($lastTest) {
                if ($lastTest->DateNextReplace) {
                    $DateNextReplace = $lastTest->DateNextReplace;
                } else {
                    $date = strtotime($lastTest->DateReplace);
                    $DateNextReplace = date("Y-m-d", strtotime("+$interval months", $date));
                }
            }

            return $DateNextReplace;
        }
    }
}
