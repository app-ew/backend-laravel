<?php
/**
 * ManufactureEdit
 */
namespace app\Models;

/**
 * ManufactureEdit
 */
class ManufactureEdit {

    /** @var float $id */
    private $id;

    /** @var float $idUser */
    private $idUser;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

    /** @var string $title */
    private $title;

    /** @var string $address */
    private $address;

}
