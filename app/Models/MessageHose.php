<?php
/**
 * MessageHose
 */
namespace app\Models;

/**
 * MessageHose
 */
class MessageHose {

    /** @var \app\Models\ManufactureView $manufacture */
    private $manufacture;

    /** @var \app\Models\ManufactureMachineryView $machinery */
    private $machinery;

    /** @var \app\Models\HoseView $hose */
    private $hose;

}
