<?php
/**
 * ManufactureContactEdit
 */
namespace app\Models;

/**
 * ManufactureContactEdit
 */
class ManufactureContactEdit {

    /** @var float $id */
    private $id;

    /** @var float $idManufacture */
    private $idManufacture;

    /** @var string $login */
    private $login;

    /** @var string $pass */
    private $pass;

    /** @var string $fIO */
    private $fIO;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

    /** @var string $title */
    private $title;

}
