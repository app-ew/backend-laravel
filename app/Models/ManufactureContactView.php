<?php
/**
 * ManufactureContactView
 */
namespace app\Models;

/**
 * ManufactureContactView
 */
class ManufactureContactView {

    /** @var float $id */
    private $id;

    /** @var float $manufacture */
    private $manufacture;

    /** @var string $login */
    private $login;

    /** @var string $fIO */
    private $fIO;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

    /** @var string $title */
    private $title;

}
