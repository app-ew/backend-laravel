<?php
/**
 * HoseView
 */
namespace app\Models;

/**
 * HoseView
 */
class HoseView {

    /** @var float $id */
    private $id;

    /** @var string $machinery */
    private $machinery;

    /** @var string $machineryGroup */
    private $machineryGroup;

    /** @var string $article */
    private $article;

    /** @var string $type */
    private $type;

    /** @var string $dn */
    private $dn;

    /** @var string $lArm */
    private $lArm;

    /** @var string $lRohrAnschlub */
    private $lRohrAnschlub;

    /** @var string $rArm */
    private $rArm;

    /** @var string $rRohrAnschlub */
    private $rRohrAnschlub;

    /** @var string $vd */
    private $vd;

    /** @var string $notice */
    private $notice;

    /** @var string $levelDanger */
    private $levelDanger;

    /** @var float $status */
    private $status;

    /** @var \DateTime $dateManufacturing */
    private $dateManufacturing;

    /** @var \DateTime $dateTest */
    private $dateTest;

    /** @var \DateTime $dateNextTest */
    private $dateNextTest;

    /** @var \DateTime $dateReplacement */
    private $dateReplacement;

}
