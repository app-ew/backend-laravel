<?php
/**
 * HoseEdit
 */
namespace app\Models;

/**
 * HoseEdit
 */
class HoseEdit {

    /** @var float $id */
    private $id;

    /** @var float $idMachinery */
    private $idMachinery;

    /** @var float $idMachineryGroup */
    private $idMachineryGroup;

    /** @var string $article */
    private $article;

    /** @var float $idType */
    private $idType;

    /** @var float $idDn */
    private $idDn;

    /** @var float $idLArm */
    private $idLArm;

    /** @var float $idLRohrAnschlub */
    private $idLRohrAnschlub;

    /** @var float $idRArm */
    private $idRArm;

    /** @var float $idRRohrAnschlub */
    private $idRRohrAnschlub;

    /** @var float $idVd */
    private $idVd;

    /** @var string $notice */
    private $notice;

    /** @var float $idLevelDanger */
    private $idLevelDanger;

    /** @var float $status */
    private $status;

    /** @var \DateTime $dateManufacturing */
    private $dateManufacturing;

    /** @var \DateTime $dateTest */
    private $dateTest;

    /** @var \DateTime $dateNextTest */
    private $dateNextTest;

    /** @var \DateTime $dateReplacement */
    private $dateReplacement;

}
