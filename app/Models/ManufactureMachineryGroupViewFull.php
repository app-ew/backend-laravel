<?php
/**
 * ManufactureMachineryGroupViewFull
 */
namespace app\Models;

/**
 * ManufactureMachineryGroupViewFull
 */
class ManufactureMachineryGroupViewFull {

    /** @var float $id */
    private $id;

    /** @var \app\Models\ManufactureContactView $contact */
    private $contact;

    /** @var \app\Models\ManufactureMachineryViewFull[] $machineryList */
    private $machineryList;

}
