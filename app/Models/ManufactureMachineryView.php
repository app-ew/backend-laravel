<?php
/**
 * ManufactureMachineryView
 */
namespace app\Models;

/**
 * ManufactureMachineryView
 */
class ManufactureMachineryView {

    /** @var float $id */
    private $id;

    /** @var string $manufacture */
    private $manufacture;

    /** @var string $machineryGroup */
    private $machineryGroup;

    /** @var \app\Models\ManufactureContactView $contact */
    private $contact;

    /** @var string $title */
    private $title;

    /** @var string $title */
    private $title;

    /** @var float $replace1 */
    private $replace1;

    /** @var float $replace2 */
    private $replace2;

    /** @var \DateTime $replace1Date */
    private $replace1Date;

}
