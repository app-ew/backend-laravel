<?php
/**
 * ManufactureView
 */
namespace app\Models;

/**
 * ManufactureView
 */
class ManufactureView {

    /** @var float $id */
    private $id;

    /** @var \app\Models\User $user */
    private $user;

    /** @var string $title */
    private $title;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

    /** @var string $address */
    private $address;

}
