<?php
/**
 * ManufactureMachineryEdit
 */
namespace app\Models;

/**
 * ManufactureMachineryEdit
 */
class ManufactureMachineryEdit {

    /** @var float $id */
    private $id;

    /** @var float $idManufacture */
    private $idManufacture;

    /** @var float $idMachineryGroup */
    private $idMachineryGroup;

     /** @var float $replace1 */
     private $replace1;

     /** @var float $replace2 */
     private $replace2;

     /** @var \DateTime $replace1Date */
     private $replace1Date;

    /** @var string $manufacture */
    private $manufacture;

    /** @var string $title */
    private $title;

    /** @var string $title */
    private $title;

    /** @var string $fIO */
    private $fIO;

    /** @var string $machineryGroup */
    private $machineryGroup;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

}
