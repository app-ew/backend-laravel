<?php
/**
 * User
 */
namespace app\Models;

/**
 * User
 */
class User {

    /** @var float $id */
    private $id;

    /** @var string $login */
    private $Login;

    /** @var string $fIO */
    private $FIO;

    /** @var string $phone */
    private $Phone;

    /** @var string $email */
    private $Email;

    /** @var string $pass */
    private $Pass;

    /** @var string $token */
    private $Token;

}
