<?php
/**
 * ManufactureMachineryGroupEdit
 */
namespace app\Models;

/**
 * ManufactureMachineryGroupEdit
 */
class ManufactureMachineryGroupEdit {

    /** @var float $id */
    private $id;

    /** @var float $idUser */
    private $idUser;

    /** @var string $title */
    private $title;

    /** @var string $address */
    private $address;

    /** @var string $login */
    private $login;

    /** @var string $fIO */
    private $fIO;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

     /** @var float $replace1 */
     private $replace1;

     /** @var float $replace2 */
     private $replace2;

     /** @var \DateTime $replace1Date */
     private $replace1Date;

}
