<?php
/**
 * ManufactureViewFull
 */
namespace app\Models;

/**
 * ManufactureViewFull
 */
class ManufactureViewFull {

    /** @var float $id */
    private $id;

    /** @var \app\Models\User $user */
    private $user;

    /** @var string $title */
    private $title;

    /** @var string $phone */
    private $phone;

    /** @var string $email */
    private $email;

    /** @var string $address */
    private $address;

    /** @var \app\Models\ManufactureMachineryGroupViewFull[] $machineryGroupList */
    private $machineryGroupList;

}
