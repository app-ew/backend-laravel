<?php
/**
 * AuthResponse
 */
namespace app\Models;

/**
 * AuthResponse
 */
class AuthResponse {

    /** @var string $token */
    private $token;

    /** @var string $refreshToken */
    private $refreshToken;

}
