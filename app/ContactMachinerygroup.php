<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class ContactMachinerygroup extends Model
{
    use Notifiable;

    protected $primaryKey ="id";

    protected $fillable = [
        "idContact",
        "idMachinerygroup",
    ];

    public function Contacts()
    {
        return $this->belongsToMany('App\Contact');
    }

    public function Machinerygroups()
    {
        return $this->belongsToMany('App\ContactMachinerygroup', 'idMachinerygroup');
    }
}
