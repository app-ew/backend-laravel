<?php

use Illuminate\Database\Seeder;

class DataHosesDnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DataHosesDn::class, 50)->create();
    }
}
