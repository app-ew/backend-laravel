<?php

use Illuminate\Database\Seeder;

class ElementReplaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ElementReplace::class, 50)->create();
    }
}
