<?php

use Illuminate\Database\Seeder;

class MachinerygroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('machinerygroups')->insert([
            'idUser' => 1,
            'idManufacture' => 1,
            'Title' => 'Цех 1',
            'Address' => '',
        ]);
        factory(App\Machinerygroup::class, 10)->create();
    }
}
