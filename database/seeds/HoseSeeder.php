<?php

use Illuminate\Database\Seeder;

class HoseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Hose::class, 500)->create();
    }
}
