<?php

use Illuminate\Database\Seeder;

class MachinerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Machinery::class, 50)->create();
    }
}
