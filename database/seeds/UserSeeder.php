<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'Login' => 'Admin',
            'FIO' => 'Admin',
            'Phone' => 'Admin',
            'email' => 'admin@site.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'token' => '',
        ]);
        factory(App\User::class, 50)->create();
    }
}
