<?php

use Illuminate\Database\Seeder;

class ElementTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('element_types')->insert([
            ['Title' => 'Hose'],
            ['Title' => 'Бак'],
            ['Title' => 'Прочее'],
        ]);
    }
}
