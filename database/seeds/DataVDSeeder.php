<?php

use Illuminate\Database\Seeder;

class DataVDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DataVD::class, 50)->create();
    }
}
