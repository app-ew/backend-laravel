<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ManufactureSeeder::class);
        $this->call(MachinerygroupSeeder::class);
        $this->call(MachinerySeeder::class);
        $this->call(ElementTypeSeeder::class);
        //$this->call(ElementSeeder::class);
        //$this->call(ElementReplaceSeeder::class);
        //$this->call(ElementTestSeeder::class);

        /*$this->call(ContactSeeder::class);
        $this->call(ContactMachinerygroupSeeder::class);
        $this->call(DataVDSeeder::class);
        $this->call(DataHosesTypeSeeder::class);
        $this->call(DataHosesDnSeeder::class);
        $this->call(DataHosesPipeConnectionSeeder::class);
        $this->call(DataHosesMountingSeeder::class);
        $this->call(DataHosesLevelDangerSeeder::class);
        $this->call(MachinerySeeder::class);
        $this->call(HoseSeeder::class);
        $this->call(HoseTestSeeder::class);*/
    }
}
