<?php

use Illuminate\Database\Seeder;

class ContactMachinerygroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ContactMachinerygroup::class, 50)->create();
    }
}
