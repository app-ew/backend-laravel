<?php

use Illuminate\Database\Seeder;

class ElementTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ElementTest::class, 50)->create();
    }
}
