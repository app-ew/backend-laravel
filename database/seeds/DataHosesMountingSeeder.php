<?php

use Illuminate\Database\Seeder;

class DataHosesMountingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DataHosesMounting::class, 50)->create();
    }
}
