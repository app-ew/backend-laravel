<?php

use Illuminate\Database\Seeder;

class DataHosesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DataHosesType::class, 50)->create();
    }
}
