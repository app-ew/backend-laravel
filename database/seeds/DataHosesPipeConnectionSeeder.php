<?php

use Illuminate\Database\Seeder;

class DataHosesPipeConnectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DataHosesPipeConnection::class, 50)->create();
    }
}
