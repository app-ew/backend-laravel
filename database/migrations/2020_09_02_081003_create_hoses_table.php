<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idMachinery')->nullable();
            $table->unsignedBigInteger('idMachinerygroup')->nullable();
            $table->string("Article")->nullable();
            $table->unsignedBigInteger("idType")->nullable();
            $table->unsignedBigInteger("idDn")->nullable();
            $table->unsignedBigInteger("idLArm")->nullable();
            $table->unsignedBigInteger("idLRohrAnschlub")->nullable();
            $table->unsignedBigInteger("idRArm")->nullable();
            $table->unsignedBigInteger("idRRohrAnschlub")->nullable();
            $table->unsignedBigInteger("idVd")->nullable();
            $table->unsignedBigInteger("idConcatenation")->nullable();
            $table->string("Notice")->nullable();
            $table->unsignedBigInteger("idLevelDanger")->nullable();
            $table->integer("Status")->nullable();
            $table->integer("IntervalReplace")->nullable();
            $table->date("DateManufacturing")->nullable();
            $table->date("DateTest")->nullable();
            $table->date("DateNextTest")->nullable();
            $table->date("DateReplacement")->nullable();

            $table->string("IntervalView")->nullable();
            $table->string("Position")->nullable();
            $table->string("InstallationLocation")->nullable();
            $table->string("Pressure")->nullable();
            $table->integer("Length")->nullable();
            $table->string("IdNumber")->nullable();
            $table->string("IdPress")->nullable();
            $table->string("InstrNo")->nullable();

            $table->string("DegreeL")->nullable();
            $table->string("DegreeR")->nullable();

            $table->string("Certificatepath")->nullable();
            $table->timestamps();
            $table->foreign('idMachinery')->references('id')->on('machineries')->onDelete('cascade');
            $table->foreign('idMachinerygroup')->references('id')->on('machinerygroups')->onDelete('cascade');
            $table->foreign('idType')->references('id')->on('data_hoses_types')->onDelete('cascade');
            $table->foreign('idDn')->references('id')->on('data_hoses_dns')->onDelete('cascade');
            $table->foreign('idLArm')->references('id')->on('data_hoses_mountings')->onDelete('cascade');
            $table->foreign('idLRohrAnschlub')->references('id')->on('data_hoses_pipe_connections')->onDelete('cascade');
            $table->foreign('idRArm')->references('id')->on('data_hoses_mountings')->onDelete('cascade');
            $table->foreign('idRRohrAnschlub')->references('id')->on('data_hoses_pipe_connections')->onDelete('cascade');
            $table->foreign('idVD')->references('id')->on('data_v_d_s')->onDelete('cascade');
            $table->foreign('idConcatenation')->references('id')->on('data_hoses_mountings')->onDelete('cascade');
            $table->foreign('idLevelDanger')->references('id')->on('data_hoses_level_dangers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoses');
    }
}
