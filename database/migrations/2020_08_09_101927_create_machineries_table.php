<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachineriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machineries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idManufacture');
            $table->unsignedBigInteger('idMachinerygroup');
            $table->string("Title");
            $table->timestamps();
            $table->foreign('idManufacture')->references('id')->on('manufactures')->onDelete('cascade');
            $table->foreign('idMachinerygroup')->references('id')->on('machinerygroups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machineries');
    }
}
