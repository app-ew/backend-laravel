<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_tests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idElement')->nullable();
            $table->integer("Status")->nullable();
            $table->string("Notice")->nullable();
            $table->date("DateTest");
            $table->date("DateNextTest")->nullable();
            $table->timestamps();

            $table->foreign('idElement')->references('id')->on('elements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_tests');
    }
}
