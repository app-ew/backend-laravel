<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('idMachinery')->nullable();
            $table->unsignedBigInteger('idMachinerygroup')->nullable();
            $table->unsignedBigInteger("idType")->nullable();

            $table->string("Article")->nullable();
            $table->longText("JSON")->nullable();

            $table->foreign('idMachinery')->references('id')->on('machineries')->onDelete('cascade');
            $table->foreign('idMachinerygroup')->references('id')->on('machinerygroups')->onDelete('cascade');
            $table->foreign('idType')->references('id')->on('element_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
