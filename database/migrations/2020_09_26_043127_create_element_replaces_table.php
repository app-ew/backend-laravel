<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementReplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_replaces', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idElement')->nullable();
            $table->string("Notice")->nullable();
            $table->date("DateReplace");
            $table->date("DateNextReplace")->nullable();
            $table->timestamps();

            $table->foreign('idElement')->references('id')->on('elements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_replaces');
    }
}
