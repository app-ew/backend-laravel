<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ContactMachinerygroup;
use Faker\Generator as Faker;

$factory->define(ContactMachinerygroup::class, function (Faker $faker) {
    return [
        'idMachinerygroup' => \App\Machinerygroup::all()->random()->id,
        'idContact' => \App\Contact::all()->random()->id,
    ];
});
