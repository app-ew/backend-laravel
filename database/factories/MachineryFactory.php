<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Machinery;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Machinery::class, function (Faker $faker) {
    return [
        'idMachinerygroup' => \App\Machinerygroup::all()->random()->id,
        'idManufacture' => \App\Manufacture::all()->random()->id,
        'Title' => $faker->jobTitle,
    ];
});
