<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DataHosesType;
use Faker\Generator as Faker;

$factory->define(DataHosesType::class, function (Faker $faker) {
    return [
        'Title' => $faker->jobTitle,
    ];
});
