<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DataVD;
use Faker\Generator as Faker;

$factory->define(DataVD::class, function (Faker $faker) {
    return [
        'Title' => $faker->jobTitle,
    ];
});
