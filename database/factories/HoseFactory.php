<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hose;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

$factory->define(Hose::class, function (Faker $faker) {
    return [
        'idMachinerygroup' => \App\Machinerygroup::all()->random()->id,
        'idMachinery' => \App\Machinery::all()->random()->id,
        'Article' => $faker->text(10),
        'idType' => \App\DataHosesType::all()->random()->id,
        'idDn' => \App\DataHosesDn::all()->random()->id,
        'idLArm' => \App\DataHosesMounting::all()->random()->id,
        'idLRohrAnschlub' => \App\DataHosesPipeConnection::all()->random()->id,
        'idRArm' => \App\DataHosesMounting::all()->random()->id,
        'idRRohrAnschlub' => \App\DataHosesPipeConnection::all()->random()->id,
        'idVd' => \App\DataVD::all()->random()->id,
        'Notice' => $faker->text(10),
        'idLevelDanger' => \App\DataHosesType::all()->random()->id,
        'Status' => $faker->randomDigitNotNull,
        'IntervalReplace' => $faker->randomDigitNotNull,
        'DateManufacturing' => $faker->dateTimeBetween($startDate = '-12 month', $endDate = '- 3 month', $timezone = null)->format('Y-m-d'),
        'DateTest' => $faker->dateTimeBetween($startDate = '- 2 month', $endDate = 'now', $timezone = null)->format('Y-m-d'),
        'DateNextTest' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 2 month', $timezone = null)->format('Y-m-d'),
        'DateReplacement' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 2 month', $timezone = null)->format('Y-m-d'),
        'Certificatepath' => '',
    ];
});
