<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DataHosesDn;
use Faker\Generator as Faker;

$factory->define(DataHosesDn::class, function (Faker $faker) {
    return [
        'Title' => $faker->jobTitle,
    ];
});
