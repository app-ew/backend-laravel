<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ElementTest;
use Faker\Generator as Faker;

$factory->define(ElementTest::class, function (Faker $faker) {
    return [
        "idElement" => \App\Element::all()->random()->id,
        "Status" => $faker->randomNumber(),
        "Notice" => $faker->text(10),
        "DateTest" => $faker->dateTimeBetween($startDate = '-12 month', $endDate = '- 3 month', $timezone = null)->format('Y-m-d'),
        "DateNextTest" => $faker->dateTimeBetween($startDate = '+3 month', $endDate = '+12 month', $timezone = null)->format('Y-m-d'),
    ];
});
