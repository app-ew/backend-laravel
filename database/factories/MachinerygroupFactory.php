<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Machinerygroup;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Machinerygroup::class, function (Faker $faker) {
    return [
        'idUser' => \App\User::all()->random()->id,
        'idManufacture' => \App\Manufacture::all()->random()->id,
        'Title' => $faker->jobTitle,
        'Address' => $faker->address,
    ];
});
