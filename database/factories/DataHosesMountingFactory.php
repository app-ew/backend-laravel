<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DataHosesMounting;
use Faker\Generator as Faker;

$factory->define(DataHosesMounting::class, function (Faker $faker) {
    return [
        'Title' => $faker->jobTitle,
    ];
});
