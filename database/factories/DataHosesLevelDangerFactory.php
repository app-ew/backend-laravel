<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DataHosesLevelDanger;
use Faker\Generator as Faker;

$factory->define(DataHosesLevelDanger::class, function (Faker $faker) {
    return [
        'Title' => $faker->jobTitle,
    ];
});
