<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ElementReplace;
use Faker\Generator as Faker;

$factory->define(ElementReplace::class, function (Faker $faker) {
    return [
        "idElement" => \App\Element::all()->random()->id,
        "Notice" => $faker->text(10),
        "DateReplace" => $faker->dateTimeBetween($startDate = '-12 month', $endDate = '- 3 month', $timezone = null)->format('Y-m-d'),
        "DateNextReplace" => $faker->dateTimeBetween($startDate = '+3 month', $endDate = '+ 12 month', $timezone = null)->format('Y-m-d'),
    ];
});
