<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DataHosesPipeConnection;
use Faker\Generator as Faker;

$factory->define(DataHosesPipeConnection::class, function (Faker $faker) {
    return [
        'Title' => $faker->jobTitle,
    ];
});
