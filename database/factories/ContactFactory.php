<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'Login' => $faker->userName,
        'Pass' => $faker->password,
        'FIO' => $faker->name,
        'Phone' => $faker->tollFreePhoneNumber,
        'Email' => $faker->unique()->safeEmail,
        'Title' => $faker->company,
    ];
});
