<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Element;
use Faker\Generator as Faker;

$factory->define(Element::class, function (Faker $faker) {
    return [
        "idMachinery" => \App\Machinery::all()->random()->id,
        "idMachinerygroup" => \App\Machinerygroup::all()->random()->id,
        "idType" => \App\ElementType::all()->random()->id,
        "Article" => $faker->text(10),
        "JSON" => "{}",
    ];
});
