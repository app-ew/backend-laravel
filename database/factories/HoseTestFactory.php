<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HoseTest;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(HoseTest::class, function (Faker $faker) {
    return [
        'idHose' => \App\Hose::all()->random()->id,
        'Status' => $faker->randomDigitNotNull,
        'Notice' => $faker->text(10),
        'DateNextTest' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 2 month', $timezone = null)->format('Y-m-d'),
    ];
});
