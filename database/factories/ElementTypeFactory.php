<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ElementType;
use Faker\Generator as Faker;

$factory->define(ElementType::class, function (Faker $faker) {
    return [
        'Title' => $faker->text(10),
    ];
});
